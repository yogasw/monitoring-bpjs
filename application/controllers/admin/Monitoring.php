<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 20.40
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("mmonitoring","monitoring");
    }

    public function index()
	{
	    $this->data = array(
	        'content' => 'monitoring/home',
            'js'      => 'home_monitoring',
            'css'     => 'home_monitoring',
            'menu'    => 'monitoring',
            'title'   => 'Data Monitoring',
            'data'    => $this->monitoring->getData()
        );

        $this->load->view('template', $this->data, FALSE);
	}

    public function update(){
        $this->data = array(
            'content' => 'monitoring/update',
            'js'      => 'home_monitoring',
            'css'     => 'home_monitoring',
            'menu'    => 'monitoring',
            'title'   => 'Data Monitoring',
            'data'    => $this->monitoring->getUpdate($this->input->get('id'))
        );
        $this->load->view('template', $this->data, FALSE);
    }
    public function save_update(){
        $this->monitoring->updateData();
        redirect('admin/monitoring');
    }
}

/* End of file Controllername.php */
