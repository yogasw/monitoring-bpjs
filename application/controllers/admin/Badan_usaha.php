<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 20.40
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Badan_usaha extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Mbadan_usaha", "badanusaha");
    }

    public function index()
	{
	    $this->data = array(
	        'content' => 'badan_usaha/home',
            'js'      => 'home_badan_usaha',
            'css'     => 'home_badan_usaha',
            'menu'    => 'badan_usaha',
            'title'   => 'Data Badan Usaha',
            //'data'    => $this->badanusaha->getData()
        );

        $this->load->view('template', $this->data, FALSE);
	}
	public function add(){
        $this->data = array(
            'content' => 'badan_usaha/add',
            'js'      => 'add_badan_usaha',
            'css'     => 'add_badan_usaha',
            'menu'    => 'badan_usaha',
            'title'   => 'Data Badan Usaha',
        );
        $this->load->view('template', $this->data, FALSE);
    }

    public function save_data(){
        $this->badanusaha->saveData();
        redirect('admin/badanusaha');
    }

    function delete(){
        $this->badanusaha->deleteData();
        redirect('admin/badanusaha');
    }

    public function update(){
        $this->data = array(
            'content' => 'badan_usaha/update',
            'js'      => 'home_badan_usaha',
            'css'     => 'home_badan_usaha',
            'menu'    => 'badan_usaha',
            'title'   => 'Data Badan Usaha',
            'data'    => $this->badanusaha->getUpdate($this->input->get('id'))
        );
        $this->load->view('template', $this->data, FALSE);
    }
    public function save_update(){
        $this->badanusaha->updateData();
        redirect('admin/badanusaha');
    }
}

/* End of file Controllername.php */
