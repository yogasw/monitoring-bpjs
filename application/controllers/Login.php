<?php

/**
 * Created by PhpStorm.
 * User: arioki
 * Date: 06/05/2019
 * Time: 04.39
 */
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("mlogin", "login");
    }

    function index()
    {

        $username = $this->input->get_post("username");
        $password = md5($this->input->get_post("password"));
        if ($this->session->userdata('username')) {
            redirect(base_url("admin"));
        }
        if ($username != "" && $password != "") {
            $action = $this->login->getData($username, $password);
            if ($action != '') {
                $session = array(
                    "username" => $action['0']['username'],
                    "level" => $action['0']['level'],
                );
                $this->session->set_userdata($session);
                redirect(base_url("admin"));
            }
        }
        $this->load->view("Login/home");
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }

}