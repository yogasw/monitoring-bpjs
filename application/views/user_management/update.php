<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 27/04/2019
 * Time: 05.29
 */
?>
<div class="panel">
    <header class="panel-heading">
        <h3 class="panel-title">
            Horizontal Form
        </h3>
    </header>
    <div class="panel-body container-fluid">
        <form action="<?php echo base_url() . "admin/usermanagement/save_update" ?>" method="post">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
            <input name="id" value="<?php echo $data->id ?>" hidden/>
            <div class="row row-lg">
                <div class="col-md-12 col-lg-6">
                    <div class="example-wrap">
                        <h4 class="example-title">Data Badan Usaha</h4>
                        <div class="example">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Username</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="username"
                                           placeholder="Masukan Username" autocomplete="off"
                                           value="<?php echo $data->username ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Password</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control" name="password"
                                           placeholder="Masukan Password" autocomplete="off"
                                           value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">level</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="level"
                                           placeholder="Masukan Level" autocomplete="off"
                                           value="<?php echo $data->level ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-12">
                    <div class="form-group row">
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-default btn-outline">Reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
