<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 20.34
 */
?>
<!-- Panel Table Tools -->
<div class="panel">
    <header class="panel-heading">
        <h3 class="panel-title">User Management</h3>
        <div class="panel-actions">
            <button type="button" class="btn btn-outline btn-default">
                <a href="<?php echo base_url() . "admin/usermanagement/add" ?>">
                    <i class="icon wb-plus" aria-hidden="true"></i>
                </a>
            </button>
        </div>
    </header>
    <div class="panel-body">
        <table class="table table-hover dataTable table-striped w-full" id="exampleTableTools">
            <thead>
            <tr>
                <th>NO</th>
                <th>username</th>
                <th>Password</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>NO</th>
                <th>username</th>
                <th>Password</th>
                <th>Action</th>
            </tr>
            </tfoot>
            <tbody>
            <?php
            $no = 1;
            foreach ($data as $val) {
                ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo $val['username'] ?></td>
                    <td><?php echo $val['password'] ?></td>
                    <td>
                        <a class="panel-action icon wb-edit" data-toggle="tooltip"
                           data-original-title="edit" data-container="body" title=""
                           href="<?php echo base_url() . "admin/usermanagement/update?id=" . $val['id'] ?>"></a>
                        <a class="panel-action icon wb-trash" data-toggle="tooltip"
                           data-original-title="move to trash" data-container="body" title=""
                           href="<?php echo base_url() . "admin/usermanagement/delete?id=" . $val['id'] ?>"></a>
                    </td>
                </tr>
                <?php $no++;
            } ?>
            </tbody>
        </table>
    </div>
</div>
<!-- End Panel Table Tools -->

