<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 27/04/2019
 * Time: 05.29
 */
?>
<div class="panel">
    <header class="panel-heading">
        <h3 class="panel-title">
            Horizontal Form
        </h3>
    </header>
    <div class="panel-body container-fluid">
        <form action="<?php echo base_url() . "admin/monitoring/save_data" ?>" method="post">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
            <div class="row row-lg">
                <div class="col-md-12 col-lg-6">
                    <div class="example-wrap">
                        <h4 class="example-title">Data Badan Usaha</h4>
                        <div class="example">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">NPP</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="npp" placeholder="NPP" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Bl-Th</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="bl_th" placeholder="Bl-Th" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Umur Piutang</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="umur_piutang" placeholder="Umur Piutang" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Jlh TK</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="jlh_tk" placeholder="Jlh TK" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Iuran</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="iuran" placeholder="Iuran" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Denda</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="denda" placeholder="Denda" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Total Iuran dan Denda</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="total_iuran_denda" placeholder="Total Iuran dan Denda" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Status Piutang</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="status_piutang" placeholder="Status Piutang" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Petugas Pemeriksa</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="petugas_pemeriksa" placeholder="Petugas Pemeriksa" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Status Kepesertaan</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="status_kepesertaan" placeholder="Status Kepesertaan" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">RO / AR</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="ro_ar" placeholder="RO / AR" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Kategori Pelanggaran</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="kategori_pelanggaran" placeholder="Kategori Pelanggaran" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Tanggal SP1</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="tanggal_sp1" placeholder="Tanggal SP1" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Nomor Surat</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="nomor_surat_sp1" placeholder="Nomor Surat" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Tanggal SP2</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="tanggal_sp2" placeholder="Tanggal SP2" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Nomor Surat</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="nomor_surat_sp2" placeholder="Nomor Surat" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Tanggal SP3</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="tanggal_sp3" placeholder="Tanggal SP3" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Nomor Surat</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="nomor_surat_sp3" placeholder="Nomor Surat" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Tanggal BAK</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="tanggal_bak" placeholder="Tanggal BAK" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="example-wrap">
                        <h4 class="example-title">Data Badan Usaha</h4>
                        <div class="example">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Tgl Pemeriksaan Data</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="tgl_pemeriksaan_data" placeholder="Tgl Pemeriksaan Data" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Tgl Pemeriksaan</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="tgl_pemeriksaan" placeholder="Tgl Pemeriksaan" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Tgl BAP / SPMI</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="tgl_bap_spmi" placeholder="Tgl BAP / SPMI" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Tgl THP</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="tgl_thp" placeholder="Tgl THP" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Tgl Penyerahan KPKNL</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="tgl_penyerahan_kpknl" placeholder="Tgl Penyerahan KPKNL" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Tgl Penyerahan Kejaksaan</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="tgl_penyerahan_kejaksaan" placeholder="Tgl Penyerahan Kejaksaan" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Tgl Rikpadu Wasnaker</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="tgl_rikpadu_wasnaker" placeholder="Tgl Rikpadu Wasnaker" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Ada Payroll / Tidak Ada Payroll*</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="payroll" placeholder="Ada Payroll / Tidak Ada Payroll*" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Patuh / NA / Cicil*</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="patuh_na_cicil" placeholder="Patuh / NA / Cicil*" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Bl-Th Terakhir</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="bl_th_terakhir" placeholder="Bl-Th Terakhir" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Potensi TK</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="potensi_tk" placeholder="Potensi TK" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Realisasi TK</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="realisasi_tk" placeholder="Realisasi TK" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-12">
                    <div class="form-group row">
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-default btn-outline">Reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
