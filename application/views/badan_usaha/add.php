<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 27/04/2019
 * Time: 05.29
 */
?>
<div class="panel">
    <header class="panel-heading">
        <h3 class="panel-title">
            Horizontal Form
        </h3>
    </header>
    <div class="panel-body container-fluid">
        <form action="<?php echo base_url()."admin/badanusaha/save_data"?>" method="post">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
        <div class="row row-lg">
            <div class="col-md-12 col-lg-6">
                <div class="example-wrap">
                    <h4 class="example-title">Data Badan Usaha</h4>
                    <div class="example">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">NPP</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="npp" placeholder="NPP" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama Badan Usaha</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="nama_badan_usaha"
                                       placeholder="Nama Badan Usaha" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">BLTH Keps
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="blth_keps"
                                       placeholder="BLTH Keps" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">BLTH Na </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="blth_na" placeholder="BLTH Na" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Rate JKK </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="rate_jkk" placeholder="Rate JKK" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Blth Terahir</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="blth_terahir" placeholder="BLTH Terahir" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Jumlah Terahir</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="jumlah_terahir" placeholder="Jumlah Terahir" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">TK Aktif Terahir</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="tk_aktif_terahir"	 placeholder="TK Aktif Terahir" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Alamat</label>
                            <div class="col-md-9"><input type="text" class="form-control" name="alamat"	 placeholder="Alamat" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kabupaten</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="kabupaten" placeholder="Kabupaten" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kode Area</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="kode_area"	 placeholder="Kode Area" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">No Telp</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="no_telp"	 placeholder="No Telp" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="example-wrap">
                    <h4 class="example-title">Data PIC</h4>
                    <div class="example">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pic_nama"	 placeholder="Nama" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">jabatan</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pic_jabatan" placeholder="Jabatan" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">No HP</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pic_no_hp" placeholder="No HP" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Email</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pic_email" placeholder="Email" autocomplete="off"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-12">
                <div class="form-group row">
                    <div class="col-md-9">
                        <button type="submit"  class="btn btn-primary">Submit </button>
                        <button type="reset" class="btn btn-default btn-outline">Reset</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
