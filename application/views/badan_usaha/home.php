<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 20.34
 */
?>
<!-- Panel Table Tools -->
<div class="panel">
    <header class="panel-heading">
        <h3 class="panel-title">Tabel Badan Usaha</h3>
        <div class="panel-actions">
            <button type="button" class="btn btn-outline btn-default">
                <a href="<?php echo base_url()."admin/badanusaha/add"?>">
                <i class="icon wb-plus" aria-hidden="true"></i>
                </a>
            </button>
        </div>
    </header>
    <div class="panel-body">
        <table class="table table-hover dataTable table-striped w-full" id="exampleTableTools">
            <thead>
            <tr>
                <th>NO</th>
                <th>NPP</th>
                <th>Nama Pemberi Kerja atau <br>Badan Usaha</th>
                <th>Kabupaten</th>
                <th>Kode Area</th>
                <th>No Telp</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>NO</th>
                <th>NPP</th>
                <th>Nama Pemberi Kerja atau <br>Badan Usaha</th>
                <th>Kabupaten</th>
                <th>Kode Area</th>
                <th>No Telp</th>
                <th>Action</th>
            </tr>
            </tfoot>

        </table>
    </div>
</div>
<!-- End Panel Table Tools -->

