<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 20.48
 */
?>

<script src="../../../global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="../../../global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="../../../global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js"></script>
<script src="../../../global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js"></script>
<script src="../../../global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js"></script>
<script src="../../../global/vendor/datatables.net-scroller/dataTables.scroller.js"></script>
<script src="../../../global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="../../../global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="../../../global/vendor/datatables.net-buttons/dataTables.buttons.js"></script>
<script src="../../../global/vendor/datatables.net-buttons/buttons.html5.js"></script>
<script src="../../../global/vendor/datatables.net-buttons/buttons.flash.js"></script>
<script src="../../../global/vendor/datatables.net-buttons/buttons.print.js"></script>
<script src="../../../global/vendor/datatables.net-buttons/buttons.colVis.js"></script>
<script src="../../../global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js"></script>
<script src="../../../global/vendor/asrange/jquery-asRange.min.js"></script>
<script src="../../../global/vendor/bootbox/bootbox.js"></script>


<script src="../../../global/js/Plugin/datatables.js"></script>

<script src="../../assets/js/core/home-badanusaha.js"></script>
