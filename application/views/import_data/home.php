<?php
/**
 * Copyright (c) 2019. Arioki Studio. All Rights Reserved. arioki1.github.io
 *
 */

/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 26/04/2019
 * Time: 20.34
 */
?>
<div class="panel">
    <header class="panel-heading">
        <h3 class="panel-title">Import Data</h3>
    </header>
    <div class="panel-body">
        <form action="<?php echo base_url() . "admin/importdata/save_data" ?>" enctype="multipart/form-data"
              method="post">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
        <div class="col-xl-12 col-md-12">
            <div class="example-wrap">
                <h4 class="example-title">Masukan file .xlsx</h4>
                <div class="example">
                    <input name="file_excel" type="file" id="input-file-now-custom-3" data-plugin="dropify"
                           data-height="100"/>
                </div>
            </div>
        </div>
        <div class="col-xl-12 col-md-12">
            <li class="mb-20">
                <button type="submit" class="btn btn-block btn-primary">Upload File</button>
            </li>
        </div>
        </form>
    </div>
</div>